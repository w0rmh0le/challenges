# Largest Number

**Determine the largest number that can be formed from a group of individual numbers.**  For example, given the numbers 50, 2, 1, 9 the largest number that can be formed is 95021.

Input will be via a text file with a variable number of positive integers and 0 on the same line separated by a single space. Your output must be the largest number that can be formed. You will be given multiple files to test with. Each file will contain one test case.

**Write your solution code to use only the largestnumber.txt file.** You can use a text editor to copy and paste data from the other test files (largestnumber2.txt and largestnumber3.txt) into largestnumber.txt.

## Test Data
|Input File        |Input Data         |Output        |
|------------------|-------------------|--------------|
|largestnumber.txt |5 2 1 9 50 56      |95650521      |
|largestnumber2.txt|201 3 77 5632 41 18|77563241320118|
|largestnumber3.txt|3412 450 0 73 8    |87345034120   |

Additional Information:
1. You do not need to edit the input data.
1. You do not need to handle exceptions.

## Bonus points
1. Add console input functionality to take in positive integer arguments and return the largest number that can be made from them
1. Add error handling