#include <stdio.h>
#include <stdlib.h>

void run_hail_seq(char*, char*, int*, long long, long long, long long);

int main(int argc, char* argv[]) {
    char *ptr;
    int i;
    long long start_num, num, l_val;

    if (argc > 1) {
        int i;
        for (i = 1; i < argc; i++) {
            run_hail_seq(argv[i], ptr, &i, start_num, num, l_val);
        }
    }
    else {
        FILE* fp;
        char buff[255];

        fp = fopen("..\\hailstone.txt", "r");

        while (fgets(buff, 255, fp) != 0) {
            run_hail_seq(buff, ptr, &i, start_num, num, l_val);
        }
        fclose(fp);
    }
    return 0;
};

void run_hail_seq(char arr[], char* ptr, int* i, long long start_num, long long num, long long l_val) {
    i = 0;
    l_val = num = start_num = strtoll(arr, &ptr, 10);
    while (num != 1) {
        num = (num % 2 == 0) ? num / 2 : (num * 3) + 1;
        l_val = (num > l_val) ? num : l_val;
        i++;
    }
    printf("Number: %lli\nIterations: %d\nLargest Value: %lli\n\n", start_num, i, l_val);
} 