'use strict';

const getMultiples = function(num) {
    let multiples = [];
    for(let b = 1; b <= num; b++) {
        if(num % b === 0) {
            multiples.push(b);
        }
    }
    return multiples;
}

let numbers = process.argv[2].split('/');
let numerator = parseInt(numbers[0]);
let denominator = parseInt(numbers[1]);

if (isNaN(numerator) || isNaN(denominator)) {
    console.log('Invalid argument "' + process.argv[2]
        + '". Fraction argument must contain two whole integers separated by a single /.\n'
        + '[node] [filename] [fraction]');
}
else {
    let numeratorMultiples = getMultiples(numerator);
    let denominatorMultiples = getMultiples(denominator);
    let commonMultiples = [];

    for (let c = 0; c < numeratorMultiples.length; c++) {
        if (denominatorMultiples.includes(numeratorMultiples[c])) {
            commonMultiples.push(numeratorMultiples[c]);
        }
    }
    commonMultiples.reverse();

    console.log((numerator /= commonMultiples[0]) + '/' + (denominator /= commonMultiples[0]));
}