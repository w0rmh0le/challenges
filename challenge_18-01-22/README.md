# challenge_18_01_22

Create a program that simplifies fractions.
Input will be a string representing the fraction, i.e. 4/28.
Numerator and denominator are guaranteed to be whole numbers.
The return value should be a string representing the simplest form of the fraction.

# Examples

| Input | Output |
| ----- | ------ |
| "4/28" | "1/7" |
| "5/30" | "1/6" |
| "12/21" | "4/7" |
