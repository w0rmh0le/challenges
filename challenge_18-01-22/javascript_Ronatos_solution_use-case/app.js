'use strict';

const simplify = require('./simplify');

console.log(
    'This is just to use Ronatos\' simplify program as an example of how node packaging works.\n');

console.log('Running simplify on my own fractions...\n');

const fractions = [
    '3/21',
    '80/1600',
    '4/200',
    '33/121'
]

fractions.forEach(fraction => {
    console.log(simplify(fraction));
});
