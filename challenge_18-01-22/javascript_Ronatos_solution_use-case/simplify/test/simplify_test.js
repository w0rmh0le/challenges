'use strict';

const fractions = require('./fractions');
const simplify = require('../');

fractions.forEach(fraction => {
    console.log('In:  ' + fraction);
    console.log('Out: ' + simplify(fraction) + '\n');
});
