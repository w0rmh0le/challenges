'use strict';

const getMultiples = function(num) {
    let multiples = [];
    for(let b = 1; b <= num; b++) {
        if(num % b === 0) {
            multiples.push(b);
        }
    }
    return multiples;
}

module.exports = (fraction) => {
    if (!fraction) {
        console.log('Simplify requires a an argument of two whole integers separated by a single /.')
        process.exit(1)
    }

    let numbers = fraction.split('/');
    let numerator = parseInt(numbers[0]);
    let denominator = parseInt(numbers[1]);

    if (isNaN(numerator) || isNaN(denominator)) {
        console.log('Invalid argument: "' + fraction+ '".\n'
        + 'Fraction argument must contain two whole integers separated by a single /.\n');
    process.exit(1);
    }

    let numeratorMultiples = getMultiples(numerator);
    let denominatorMultiples = getMultiples(denominator);
    let commonMultiples = [];

    for (let c = 0; c < numeratorMultiples.length; c++) {
        if (denominatorMultiples.includes(numeratorMultiples[c])) {
            commonMultiples.push(numeratorMultiples[c]);
        }
    }
    commonMultiples.reverse();

    return (numerator /= commonMultiples[0]) + '/' + (denominator /= commonMultiples[0]);
}
