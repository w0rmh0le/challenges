# Challenges

I'm going to try to post a new challenge every week. and hopefully, you w0rms keep up.

## Submission

I will create a new directory for each challenge named after the date I post the challenge. Inside of that directory will be a `README.md` containing the challenge information. To submit your challenge solution, `push` a directory into the relevant challenge directory in the following format:

- `<language>_<username>_solution`

example:

If I were to submit a solution to the January 1st, 2018 challenge, the path to my challenge directory would be:

- `w0rmh0le/challenges/challenge_18-01-26/c++_johnryanaudio_solution/`

I don't care how you structure your solution inside of your directory, but hopefully you take the time to make it easy for the rest of us to view and run your solution.


Thanks for participating!
